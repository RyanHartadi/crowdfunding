import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StyleSheet, Text, View } from 'react-native'
import {Intro , SplashScreen , Login , Account , EditAkun, Register, VerificationCode, UbahPassword} from '../pages';


const Stack = createStackNavigator()
const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Account" component={Account} />
        <Stack.Screen name="EditAkun" component={EditAkun} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="VerificationCode" component={VerificationCode} />
        <Stack.Screen name="UbahPassword" component={UbahPassword} />
    </Stack.Navigator>
)

const Routes = () => {

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)

    },[])

    if(isLoading) {
        return <SplashScreen />
    }

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}
export default Routes
