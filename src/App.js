import React, { useEffect } from 'react';
import Routes from './routes'
import firebase from '@react-native-firebase/app';

var firebaseConfig = {
  apiKey: "AIzaSyCAXYr_8iMAm7UAKhiudMV8xayFq7aaSes",
  authDomain: "sanbercode-caa75.firebaseapp.com",
  databaseURL: "https://sanbercode-caa75.firebaseio.com",
  projectId: "sanbercode-caa75",
  storageBucket: "sanbercode-caa75.appspot.com",
  messagingSenderId: "335851805933",
  appId: "1:335851805933:web:3b0cb3e19b52e17dc640e4",
  measurementId: "G-BDGC7JRBL0"
};
  // Initialize Firebase

  if(!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
  }
  
  const App: () => React$Node = () => {
  
    return (
      <Routes />
    );
  };

export default App;