import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import colors from '../colors'

const ButtonStyle = ({onPress , title , type}) => {
    if(type == "Register") {
        return (
            <TouchableOpacity style={styles.containerRegister} onPress={onPress}>
                <Text style={styles.textRegister}>{title}</Text>
            </TouchableOpacity>
        )
    }else if(type == "LoggedNow"){
        return (
            <TouchableOpacity style={styles.containerLogged} onPress={onPress}>
                <Text style={styles.textLogged}>{title}</Text>
            </TouchableOpacity>
        )
    }
    else {
        return (
            <TouchableOpacity style={styles.container} onPress={onPress}>
                <Text style={styles.text}>{title}</Text>
            </TouchableOpacity>
        )
    }
   
}

export default ButtonStyle

const styles = StyleSheet.create({
    container : {
        borderWidth : 1,
        width : 200,
        height : 40,
        borderColor : colors.white,
        backgroundColor : colors.white,
        borderRadius : 20
    },
    containerLogged : {
        borderWidth : 1,
        width : 200,
        height : 40,
        borderColor : colors.white,
        backgroundColor : colors.blue,
        borderRadius : 20
    },
    containerRegister : {
        borderWidth : 1,
        width : 200,
        height : 40,
        borderColor : colors.white,
        borderRadius : 20
    },
    text : {
        textAlign : 'center',
        textAlignVertical : 'center',
        flex : 1,
        fontSize : 15,
        fontWeight : 'bold',
        color : 'black'
    },
    textLogged : {
        textAlign : 'center',
        textAlignVertical : 'center',
        flex : 1,
        fontSize : 15,
        fontWeight : 'bold',
        color : 'white'
    },
    textRegister : {
        textAlign : 'center',
        textAlignVertical : 'center',
        flex : 1,
        fontSize : 15,
        fontWeight : 'bold',
        color : 'white'
    }
})
