import React , {useState} from 'react'
import { Alert, StyleSheet, Text, View } from 'react-native'
import {ScrollView, TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {ButtonStyle, colors, Gap} from '../../components';
import api from '../../api';
import Axios from 'react-native-axios';
const UbahPassword = ({route , navigation}) => {
    
    const [password, setPassword] = useState('');
    const [retype, setRetype] = useState('');
    const { email } = route.params;
    console.log(email);
    const kirim = () => {
        console.log(password , retype)
        let data = {
            email: email,
            password: password,
            password_confirmation: retype,
          };
          Axios.post(`${api}/auth/update-password`, data, {
            timeout: 2000,
          })
            .then((res) => {
                console.log("sukses =>" ,res)
                navigation.navigate('Login')
            })
            .catch((err) => {
                console.log("err =>" ,err)
              Alert.alert(
                'Error',
                'Maaf Email / Password Anda Salah',
                [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: false},
              );
            });
    }
    return (
        <View style={styles.container}>
            <Text style={{fontWeight : 'bold'}}>Tentukan kata sandi baru untuk keamanan akun kamu</Text>
            <Gap height={80}/>
            <TextInput
                style={styles.parent}
                placeholder="Password"
                value={password}
                secureTextEntry
                onChangeText={(password) => setPassword(password)}
            />
               <Gap height={30}/>
            <TextInput
              style={styles.parent}
              placeholder="Konfirmasi Password"
              secureTextEntry
              value={retype}
              onChangeText={(retype) => setRetype(retype)}
            />
              <Gap height={30}/>
              <View style={{flex : 1, alignItems : 'center'}}>
                <ButtonStyle  
                    title="Simpan"
                    onPress = {() => kirim()}
                    type="LoggedNow"/>
              </View>
        </View>
    )
}

export default UbahPassword

const styles = StyleSheet.create({
    container : {
        flex : 1 ,
        backgroundColor : colors.white,
        paddingHorizontal: 20,
        paddingTop: 50,
    },
    parent: {
        borderWidth: 0.1,
        width: 350,
        height: 40,
        borderRadius: 3,
        backgroundColor: colors.white,
        paddingHorizontal: 15,
    }
})
