import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  Dimensions,
  Alert,
} from 'react-native';
import {ScrollView, TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {ButtonStyle, colors, Gap} from '../../components';
import Axios from 'react-native-axios';
import Asyncstorage from '@react-native-async-storage/async-storage';
import api from '../../api';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes
} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth'
import TouchID from 'react-native-touch-id';


const height = Dimensions.get('window').height;

const config = { 
  title : 'Authentication Required',
  imageColor : colors.blue,
  imageErrorColor : 'red',
  sensorDescription : 'Touch Sensor',
  sensorErrorDescription : 'Failed',
  cancelText : 'Cancel'
}

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const saveToken = async (token) => {
    try {
      await Asyncstorage.setItem('token', token);
    } catch (err) {
      console.log(err);
    }
  };


  useEffect(() => {
    configureGoogleSignIn();
  }, []);


  const configureGoogleSignIn = () => {
   GoogleSignin.configure({
       webClientId : '335851805933-iqveqlcuapnv24oqclaputqq7dco9nnk.apps.googleusercontent.com',
       offlineAccess : false
   })
  };

  const signInWithGoogle = async () => {
    try {
      const { idToken } = await GoogleSignin.signIn();
      console.log('signInWithGoogle', idToken);


      const credential = auth.GoogleAuthProvider.credential(idToken)

      auth().signInWithCredential(credential)

      navigation.navigate('Account')
    } catch (error) {
      console.log('signInWithGoogle err', error);
    }
  };

  const signInWithFingerprint = () => {
    TouchID.authenticate('' , config).then(success => {
      alert("Auth Success")
      navigation.navigate('Account')
    }).catch(error => {
      alert("Failed")
    })
  }

  const Login = () => {
    let data = {
      email: email,
      password: password,
    };
    Axios.post(`${api}/auth/login`, data, {
      timeout: 2000,
    })
      .then((res) => {
        saveToken(res.data.data.token);
        navigation.navigate('Account');
        setEmail('');
        setPassword('');
      })
      .catch((err) => {
        Alert.alert(
          'Error',
          'Maaf Email / Password Anda Salah',
          [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
      });
  };

  return (
   
 <View style={styles.container}>
      <StatusBar backgroundColor={'#3a86ff'} barStyle="light-content" />
      <Image source={require('../../assets/Dummy1.jpg')} style={styles.img} />
      <Text style={styles.text}>Welcome Back</Text>
      <Gap height={10} />
      <Text style={styles.text2}>Sign to Continue</Text>
      <Gap height={20} />
      <View style={{alignItems: 'center'}}>
        <Text>Email</Text>
        <Gap height={10} />
        <TextInput
          style={styles.parent}
          value={email}
          onChangeText={(email) => setEmail(email)}
        />
        <Gap height={20} />
        <Text>Password</Text>
        <Gap height={10} />
        <TextInput
          style={styles.parent}
          secureTextEntry
          value={password}
          onChangeText={(password) => setPassword(password)}
        />
        <Gap height={20} />
        <ButtonStyle
          title="Login Now"
          type="LoggedNow"
          onPress={() => Login()}
        />

        <Gap height={30} />
        <View style={styles.garis} />
        <Gap height={10} />
        <Text>Login Dengan</Text>
        <Gap height={10} />
        <GoogleSigninButton
        onPress={() =>  signInWithGoogle()}
          style={{width: '100%', height: 40}}
          size={GoogleSigninButton.Size.Wide}
          color={GoogleSigninButton.Color.Dark}
        />
        <Gap height={10}/>
        <TouchableOpacity style={styles.btn} onPress={() => signInWithFingerprint()}>
            <Text style={styles.txtbtn}>Sign In With Fingerprint</Text>
        </TouchableOpacity>
      </View>
    </View>
   
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 10,
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    color: colors.blue,
    alignSelf: 'center',
  },
  text2: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.grey,
    alignSelf: 'center',
  },
  img: {
    width: 200,
    height: 200,
    alignSelf: 'center',
    marginTop: height * 0.05,
  },
  parent: {
    borderWidth: 0.1,
    width: 250,
    height: 40,
    borderRadius: 3,
    backgroundColor: colors.white,
    paddingHorizontal: 15,
  },
  garis: {borderWidth: 0.5, width: 200},
  btn: {
    borderWidth: 0.5, 
    width: 350, 
    height: 40, 
    backgroundColor : colors.blue
  },
  txtbtn: {textAlign: 'center', textAlignVertical: 'center', flex: 1 , color : colors.white},
});
