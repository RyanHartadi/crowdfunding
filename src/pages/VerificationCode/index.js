import React  , {useState}from 'react'
import { Alert, StyleSheet, Text, View } from 'react-native'
import { TextInput } from 'react-native-gesture-handler';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { ButtonStyle, colors, Gap } from '../../components'
import api from '../../api';
import Axios from 'react-native-axios';
const VerificationCode = ({route , navigation}) => {
    const [code, setCode] = useState('');
    const { email } = route.params;
    const verif = () => {
      console.log("ini code=>" ,code);
      let data = {
        otp: code,
      };
      Axios.post(`${api}/auth/verification`, data, {
        timeout: 2000,
      })
        .then((res) => {
            console.log("sukses =>" ,res)
            navigation.navigate('UbahPassword' , {
                email : email
            })
        })
        .catch((err) => {
            console.log("err =>" ,err)
            // navigation.navigate('UbahPassword')
          Alert.alert(
            'Error',
            'Maaf Kode OTP Salah',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
          );
        });
    }
    return (
        <View style={styles.container}>
            <View style={{paddingTop : 80}}>
                <Text style={{fontWeight : 'bold'}}>Perjalanan Kebaikanmu Dimulai Disini</Text>
                <Text>Masukkan 6 digit kode yang kami kirim ke</Text>
                <Text style={{fontWeight : 'bold'}}>{email}</Text>
            </View>
            <View style={{alignItems : 'center'  , flex :1 , paddingTop : '20%'}}>
                {/* <TextInput
                    style={styles.parent}
                    value={code}
                    onChangeText={(code) => setCode(code)}
                />
                <Gap height={30}/> */}
                <OTPInputView
                    style={{width: '80%', height: 200}}
                    pinCount={6}
                    autoFocusOnLoad
                    codeInputFieldStyle={styles.underlineStyleBase}
                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                    onCodeFilled = {(code1) => {
                        console.log(`Code is ${code1}, you are good to go!`)
                        setCode(code1);
                    }}
                />
                <ButtonStyle  
                    title="VERIFIKASI"
                    onPress={() => verif()}
                    type="LoggedNow"/>
            </View>
        </View>
    )
}

export default VerificationCode

const styles = StyleSheet.create({
    container : {
        flex : 1 ,
        backgroundColor : colors.white,
        paddingHorizontal: 20,
        paddingTop: 10,
    },
    parent: {
        borderWidth: 0.1,
        width: 350,
        height: 40,
        borderRadius: 3,
        backgroundColor: colors.white,
        paddingHorizontal: 15,
       
    },
    borderStyleBase: {
        width: 30,
        height: 45
      },
    
      borderStyleHighLighted: {
        borderColor: "#03DAC6",
      },
    
      underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
      },
    
      underlineStyleHighLighted: {
        borderColor: "#03DAC6",
      },
})
