import React , {useEffect , useRef, useState } from 'react'
import { StyleSheet, Text, View , Dimensions , StatusBar , Image , Modal , TouchableOpacity} from 'react-native'
import api from '../../api'
import Axios from 'react-native-axios';
import Asyncstorage from '@react-native-async-storage/async-storage';
import { TextInput } from 'react-native-gesture-handler';
import {ButtonStyle, colors, Gap} from '../../components';
import { RNCamera } from 'react-native-camera'; 

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/Feather';
const height = Dimensions.get('window').height;
const EditAkun = ({route , navigation}) => {

    let input = useRef(null)
    let camera = useRef(null)
    const [editable , setEditable] = useState(false)
    const [token , setToken] = useState('')
    const { email , photoku} = route.params;
    const [name , setName] = useState(route.params.name);
    const [isVisible , setIsVisible] = useState(false)
    const [type , setType] = useState('back')
    const [photo , setPhoto] = useState(null)

    const toogleCamera = () => {
        setType(type == 'back' ? 'front' : 'back')
    }
    const editData = () => {
        setEditable(!editable)
    }

    const takePicture = async () => {
        const options = {quality : 0.5 , base64 : true}
        if(camera) {
            const data = await camera.current.takePictureAsync(options)
            setPhoto(data)
            setIsVisible(false)
            console.log("cam =>" , data.uri);
         
        }
    }
    const onSavePress = () => {
        const formData = new FormData()
        formData.append('name' , name);
        formData.append('photo' , {
            uri : photo.uri,
            name : 'photo.jpg',
            type : 'image/jpg'
        })
        Axios.post(`${api}/profile/update-profile` , formData , {
            timeout : 2000,
            headers : {
                'Authorization' : "Bearer" + token,
                Accept : 'application/json',
                'Content-Type' : 'multipart/form-data'
            }
        }).then((res) => {
            console.log(res)
            alert(" Success")
        }).catch((err) => {
            console.log(err)
            alert("Error")
        })
    }
    async function getToken  () {
        try {
            const token = await Asyncstorage.getItem('token')
            // console.log("tokennya" , token)
            if(token !== null){
                    setToken(token)
            }
        }catch(err) {
            console.log(err)
        }
    }
    
    
    useEffect(() => {
        getToken()
    })
    
    const renderCamera = () => {
        return(
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{flex:1}}>
                    <RNCamera style={{flex:1}} type={type} ref={camera}>
                           <View style={styles.camFlipContainer}>
                               <TouchableOpacity style={styles.btnFlip} onPress={() => toogleCamera()}>
                                   <MaterialCommunityIcons name="rotate-3d-variant" size={15} style={styles.flip}/>
                               </TouchableOpacity>
                           </View>    
                           <View style={styles.round}/>
                           <Gap height={30}/>
                           <View style={styles.rectangle}/>
                           <View style={styles.btnTakeContainer}>
                               <TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
                                   <Icon name="camera" size={30} style={styles.cam}/>
                               </TouchableOpacity>
                           </View>

                     </RNCamera>
                </View>
            </Modal>
        )
    }
    
    return (
        <View style={styles.container}>
      <StatusBar backgroundColor={'#3a86ff'} barStyle="light-content" />
      <TouchableOpacity onPress={() => setIsVisible(true)}>
      <Image source={route.params.photoku !== null && photo == null ? {uri : `${photo}`} : photo.uri} style={styles.img} />

      </TouchableOpacity>
   
      <Gap height={20} />
      <View style={{alignItems: 'center'}}>
        <Text>Email</Text>
        <Gap height={10} />
        <TextInput
          style={styles.parent}
          ref={input}
          value={name}
          onChangeText={(value)=>setName(value)}
        
        />
        <Gap height={20} />
        <Text>Password</Text>
        <Gap height={10} />
        <TextInput
          style={styles.parent}
          editable={editable}
          ref={input}
          value={email}
          onChangeText={(value)=>setName(value)}
        />
        <Gap height={20} />
        <ButtonStyle
          title="Edit Account"
          type="LoggedNow"
          onPress={() => onSavePress()}
        />

      </View>
      {renderCamera()}
    </View>
   
    )
}

export default EditAkun

const styles = StyleSheet.create({

  container: {
    backgroundColor: colors.white,
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 10,
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
    color: colors.blue,
    alignSelf: 'center',
  },
  text2: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.grey,
    alignSelf: 'center',
  },
  img: {
    width: 200,
    height: 200,
    alignSelf: 'center',
    borderRadius : 100,
    marginTop: height * 0.05,
  },
  parent: {
    borderWidth: 0.1,
    width: 250,
    height: 40,
    borderRadius: 3,
    backgroundColor: colors.white,
    paddingHorizontal: 15,
  },
  garis: {borderWidth: 0.5, width: 200},
  btn: {
    borderWidth: 0.5, 
    width: 350, 
    height: 40, 
    backgroundColor : colors.blue
  },
  txtbtn: {textAlign: 'center', textAlignVertical: 'center', flex: 1 , color : colors.white},
  btnTake : {
      backgroundColor : colors.white ,
      width : 90, 
      height : 90 , 
      borderRadius : 45,
      alignSelf : 'center',
      justifyContent : 'flex-end'
      
  },
  btnFlip : {
    backgroundColor : colors.white ,
    width : 60, 
    height : 60 , 
    borderRadius : 45,
    marginTop : 80,
    marginLeft : 20
  },
  cam : {
      flex:1,
      color : 'black',
     alignSelf : 'center',
     textAlignVertical : 'center'
  },
  flip : {
    flex:1,
    color : 'black',
   alignSelf : 'center',
   textAlignVertical : 'center'
  },
  btnTakeContainer : {
      flex :1,
      justifyContent : 'flex-end',
      marginBottom : 60
  },
  round : {
      borderRadius : 100,
      width : 120,
      height : 120,
      borderWidth : 1,
      borderColor : colors.white,
      alignSelf : 'center',
      textAlignVertical : 'center'
  },
  rectangle : {
    borderRadius : 100,
    width : 150,
    height : 250,
    borderWidth : 1,
    borderColor : colors.white,
    alignSelf : 'center',
    textAlignVertical : 'center'
  }
})
