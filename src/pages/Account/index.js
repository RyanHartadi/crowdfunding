import React, { useEffect ,useState} from 'react'
import { StyleSheet, Text, SafeAreaView, View ,Image , TouchableOpacity, Modal } from 'react-native';
import { Dimensions } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Octicons from 'react-native-vector-icons/Octicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/Feather';
import { Gap } from '../../components';
import Axios from 'react-native-axios';
import Asyncstorage from '@react-native-async-storage/async-storage';
import api from '../../api'
import { GoogleSignin } from '@react-native-community/google-signin';
import { RNCamera } from 'react-native-camera';

const Account = ({navigation}) => {
    const [data, setData] = useState({});
    const [userInfo , setUserInfo] = useState(null)
    async function getToken  () {
        try {
            const token = await Asyncstorage.getItem('token')
            // console.log("tokennya" , token)
            return getData(token)
        }catch(err) {
            console.log(err)
        }
    }
    const getData = (token) => {
        Axios.get(`${api}/profile/get-profile`,{
            timeout : 2000,
            headers : {
                'Authorization' : 'Bearer' + token
            }
        }).then((res) => {
            setData(res.data.data.profile)
        }).catch((err) => {
            console.log(err)
        })
    }

    const getCurrentUser = async () => {
        const userInfo = await GoogleSignin.signInSilently()
        console.log(userInfo)
        setUserInfo(userInfo)
    }
    useEffect(() => {
        getToken()
        // getCurrentUser()
    })

   

    const Logout = async () => {
        try {
            // await GoogleSignin.revokeAccess()
            // await GoogleSignin.signOut()
            await Asyncstorage.removeItem("token")
            navigation.navigate('Login')
        }catch(err){
            console.log(err)
        }
    }
    return (
        <View style={styles.views}>
            
            <View style={styles.parent2}>
                <Gap height={20}/>
                <TouchableOpacity style={styles.flex} onPress={() => navigation.navigate('EditAkun' , {
                    name : data.name,
                    photoku : data.photo,
                    email : data.email,
                })}>  
                    <Image source={{ uri:`https://crowdfunding.sanberdev.com/${data.photo}`}} style={styles.image} />
                    {/* <Image source={{ uri: userInfo && userInfo.user && userInfo.user.photo}} style={styles.image} /> */}
                    {/* <Text style={styles.text}>{ userInfo && userInfo.user && userInfo.user.name}</Text>     */}
                    <Text style={styles.text}>{ data.name}</Text>    
                </TouchableOpacity>
                <Gap height={20}/>
            </View>
            <View style={{height : 1 , backgroundColor : '#cfcdc8'}}></View>

            <View style={styles.parent2}>
                <View style={styles.flex}>
                    <AntDesign  name="wallet" size={25} color="black" />
                        <Text style={styles.text2}>Saldo</Text>
                        <View style={{alignItems : 'flex-end' , flex:1}}>  
                            <Text style={styles.text2}>Rp.120.000.000</Text>
                        </View>
                </View>
            </View>
            <View style={{height : 1 , backgroundColor : '#cfcdc8'}}></View>
            {/* Settings */}
            <View style={styles.parent2}>
                <View style={styles.flex}>
                    <AntDesign  name="setting" size={25} color="black" />
                    <Text style={styles.text2}>Pengaturan</Text>
                </View> 
            </View>
            <View style={{height : 1 , backgroundColor : '#cfcdc8'}}></View>
             {/* Bantuan */}
            <View style={styles.parent2}>
                <View style={styles.flex}>
                    <Octicons  name="question" size={25} color="black" />
                    <Text style={styles.text2}>Bantuan</Text>    
                </View>
            </View>
            <View style={{height : 1 , backgroundColor : '#cfcdc8'}}></View>
           
            <View style={styles.parent2}>
                <View style={styles.flex}>
                        <MaterialCommunityIcons  name="text-box-multiple" size={25} color="black" />
                        <Text style={styles.text2}>Syarat & Ketentuan</Text>        
                </View>
            </View>
            <View style={{height : 1 , backgroundColor : '#cfcdc8'}}></View>
            
            <TouchableOpacity style={styles.parent2} onPress={() => Logout()}>
                <View style={styles.flex}>
                    <AntDesign  name="logout" size={25} color="black" />
                    <Text style={styles.text2}>Keluar</Text>  
                </View>
                <Gap height={20}/>
            </TouchableOpacity>
        </View>
     
    )
}

export default Account
const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
    views: {
      backgroundColor : 'white',
      flex:1,
      padding : 20
    },
    flex: {
        flexDirection : 'row'
    },
    parent: {
        padding : 20,
    },
    parent2 :{ 
        padding : 20,
    },
    title : {
        color : 'white',
        fontSize : 20
    },
    image: {
        width: 75,
        height: 75,
        borderRadius: 75 / 2,
    },
    text: {
        textAlign: 'center',
        textAlignVertical: "center",
        marginLeft : windowWidth * 0.1,
        fontSize : 20,
        fontWeight : 'bold'
    },
    text2 : {
        fontSize : 18,
        marginLeft : windowWidth * 0.1,
        fontWeight : '300'
    }
  });
  
