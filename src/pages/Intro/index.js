import React from 'react'
import { View, Text, Image, StatusBar, SafeAreaView , StyleSheet } from 'react-native'

//import module  react native app intro slider
import AppIntroSlider from 'react-native-app-intro-slider'
import { ButtonStyle, Gap } from '../../components'

// data yang akan kita tampilkan sebagai onboarding aplikasi
const data = [
    {
        id: 1,
        image: require('../../assets/Dummy1.jpg'),
        description: 'Selamat Datang Di Aplikasi Crownfunding'
    },
    {
        id: 2,
        image: require('../../assets/Dummy2.jpg'),
        description: 'Aplikasi Ini Bertujuan Berdonasi Sesama'
    },
    {
        id: 3,
        image: require('../../assets/Dummy3.jpg'),
        description: 'Ditengah Covid 19 ini Donasi Anda Sangat diperlukan'
    }
]

const Intro = ({ navigation }) => {

    //tampilan onboarding yang ditampilkan dalam renderItem
    const renderItem = ({ item }) => {
        return (
            <View style={styles.listContainer}>
                <View style={styles.listContent}>
                    <Image source={item.image} style={styles.imgList} resizeMethod="auto" resizeMode="contain" />
                </View>
                <Gap height={30}/>
                <Text style={styles.textList}>{item.description}</Text>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <StatusBar backgroundColor={'#3a86ff'} barStyle="light-content" />
                <View style={styles.textLogoContainer}>
                    <Text style={styles.textLogo}>Crowd Funding</Text>
                </View>
                <View style={styles.slider}>
                    {/* contoh menggunakan component react native app intro slider */}
                    <AppIntroSlider
                        data={data} //masukan data yang akan ditampilkan menjadi onBoarding, dia bernilai array
                        renderItem={renderItem} // untuk menampilkan onBoarding dar data array
                        renderNextButton={() => null}
                        renderDoneButton={() => null}
                        activeDotStyle={styles.activeDotStyle}
                        keyExtractor={(item) => item.id.toString()} 
                    />
                </View>
                <View style={styles.btnContainer}>
                <ButtonStyle title="Login Now" onPress={() => navigation.navigate('Login')} />
                <Gap height={20}/>
                <ButtonStyle title="Register Account" type="Register" onPress={() => navigation.navigate('Register')} />
                </View>
            </View>
        </SafeAreaView>
    )
}

export default Intro

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3a86ff'
    },
    textLogoContainer: {
        marginTop: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textLogo: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#ffffff'
    },
    slider: {
        flex: 1
    },
    btnContainer: {
        marginBottom: 15,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    btnLogin: {
        height: 35,
        width: '90%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff'
    },
    btnTextLogin: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#3a86ff'
    },
    btnRegister: {
        height: 35,
        width: '90%',
        borderWidth: 1.5,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#ffffff',
        backgroundColor: 'transparent'
    },
    btnTextRegister: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#ffffff'
    },
    listContainer: {
        marginTop: 25,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    listContent: {
        marginTop: 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgList: {
        width: 330,
        height: 330
    },
    textList: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#ffffff'
    },
    activeDotStyle: {
        width: 20,
        backgroundColor: '#ffffff'
    }
})