import Login from './Login';
import SplashScreen from './SplashScreen'
import Intro from './Intro'
import Account from './Account'
import EditAkun from './EditAkun'
import Register from './Register'
import VerificationCode from './VerificationCode'
import UbahPassword from './UbahPassword'


export {
    Login , SplashScreen , Intro , Account, EditAkun , Register , VerificationCode , UbahPassword
}